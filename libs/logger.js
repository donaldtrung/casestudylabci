var winston = require('winston'),
    dotenv = require('dotenv');

//Load .env
dotenv.load();

var logger = new winston.Logger({
    level: 'info',
    json: false,
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            filename: process.env.LOGFILE,
            json: false
        })
    ]
});

var jsonLogger = new winston.Logger({
    level: 'info',
    json: true,
    timestamp: false,
    transports: [
        new (winston.transports.Console)(),
        new (winston.transports.File)({
            filename: process.env.JSON_LOGFILE ? process.env.JSON_LOGFILE : 'logs/json_systems.log',
            // handleExceptions: true,
            json: true
        })
    ]
});

exports.dump = function(obj) {
    console.log(obj);
};

exports.logError = function(message, obj, error) {
    logger.error(message, obj, error);
};

exports.logInfo = function(message, obj) {
    let debugMod = process.env.DEBUG;
    console.log('Debug mode.... ', debugMod);
    logger.info(message, obj);
};

exports.logInfoJS = function(message, obj) {
    jsonLogger.info(message, obj);
};
