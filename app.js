console.log('herer');

const path = require('path');
var express = require('express');
var exphbs  = require('express-handlebars');
var app = express();

global.rootDir = __dirname;

app.engine('handlebars', exphbs());
app.use(express.static(__dirname + '/public'));

app.set('view engine', 'handlebars');

var bodyParser = require('body-parser');
require("./storage/globalVariables");

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

require("./routes/index.js")(app, express);
require("./routes/book.js")(app, express);
require("./routes/author.js")(app, express);

require("./routes/template.js")(app, express);
require("./routes/dynamic.js")(app, express);

app.set("port", process.env.PORT || 3000);
app.set("host", process.env.HOST || "localhost");

app.listen(app.get("port"), function() {
    console.log(
        "%s server listening at http://%s:%s",
        process.env.NODE_ENV,
        app.get("host"),
        app.get("port")
    );
});
process.on('uncaughtException', function (error) {
});