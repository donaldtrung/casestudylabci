'use strict';
module.exports = (sequelize, DataTypes) => {
    const RoutePermission = sequelize.define('RoutePermission', {}, {});
    RoutePermission.associate = function (models) {
        models.RoutePermission.belongsTo(models.Permission, {foreignKey: 'permissionId', targetKey: 'id'});
        models.RoutePermission.belongsTo(models.Route, {foreignKey: 'routeId', targetKey: 'id'});
    };
    return RoutePermission;
};