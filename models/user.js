'use strict';
module.exports = (sequelize, DataTypes) => {
    const User = sequelize.define('User', {
        username: DataTypes.STRING,
        password: DataTypes.STRING
    }, {});
    User.associate = function (models) {
        // associations can be defined here
        models.RoleRoute.belongsTo(models.Role, {foreignKey: 'roleId', targetKey: 'id'});
    };
    return User;
};