let dotenv = require('dotenv');
let permissionController = require('../controllers/permissionController');

dotenv.load();

let appRouter = function (app, express) {
    let router = express.Router();

    // router.get('/:param1', permissionController.validatePermission, function (request, response) {
    //     response.send("Hey you have permission access");
    // });
    //
    // router.post('/:param1', permissionController.validatePermission, function (request, response) {
    //     response.send("Hey you have permission access");
    // });

    router.get('/:param1/:param2', permissionController.validatePermission, function (request, response) {
        response.send("Hey you have permission access");
    });

    router.post('/:param1/:param2', permissionController.validatePermission, function (request, response) {
        response.send("Hey you have permission access");
    });

    router.put('/:param1/:param2', permissionController.validatePermission, function (request, response) {
        response.send("Hey you have permission access");
    });

    router.delete('/:param1/:param2', permissionController.validatePermission, function (request, response) {
        response.send("Hey you have permission access");
    });

    router.get('/:param1/:param2/:param3', permissionController.validatePermission, function (request, response) {
        response.send("Hey you have permission access");
    });

    router.post('/:param1/:param2/:param3', permissionController.validatePermission, function (request, response) {
        response.send("Hey you have permission access");
    });

    router.put('/:param1/:param2/:param3', permissionController.validatePermission, function (request, response) {
        response.send("Hey you have permission access");
    });

    router.delete('/:param1/:param2/:param3', permissionController.validatePermission, function (request, response) {
        response.send("Hey you have permission access");
    });

    app.use('/', router);
};

module.exports = appRouter;



