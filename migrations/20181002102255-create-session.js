'use strict';
module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.createTable('Sessions', {
            id: {
                allowNull: false,
                autoIncrement: true,
                primaryKey: true,
                type: Sequelize.INTEGER
            },
            userId: // name of the key we're adding
                {
                    type: Sequelize.INTEGER,
                    foreignKey: true,
                    references: {
                        model: 'Users', // name of Target model
                        key: 'id' // key in Target model that we're referencing
                    },
                    onUpdate: 'CASCADE',
                    onDelete: 'CASCADE'
                },
            session: {
                type: Sequelize.STRING(1024)
            },
            createdAt: {
                allowNull: false,
                type: Sequelize.DATE
            },
            updatedAt: {
                allowNull: false,
                type: Sequelize.DATE
            }
        });
    },
    down: (queryInterface, Sequelize) => {
        return queryInterface.dropTable('Sessions');
    }
};