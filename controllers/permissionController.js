let dotenv = require('dotenv');
let logger = require('../libs/logger');
let responseUtil = require('../utils/responseUtil');
let permissionService = require('../services/permissionService');

dotenv.load();

exports.validatePermission = function (request, response, next) {
    let session = request.headers.session;

    if (!session) {
        response.send(responseUtil.response(null, responseUtil.FAIL_AUTHENTICATE_CODE, responseUtil.FAIL_AUTHENTICATE_MESSAGE));
    }

    permissionService.isValidSession(session).then(function (userId) {
        global.userId = userId;

        permissionService.getRouteData(request.method, userId).then(function (routes) {
            for (let index = 0; index < routes.length; index++) {
                let routeData = routes[index];

                if (!routeData || !routeData.role || !routeData.role.roleRoute) {
                    return response.send(responseUtil.response(null, responseUtil.FAIL_AUTHENTICATE_CODE, responseUtil.FAIL_AUTHENTICATE_MESSAGE));
                }

                let arrRouteValid = routeData.role.roleRoute;
                let arrRouteUrl = request.originalUrl.split("/");

                // let canAccess = permissionService.checkRoutePermission(arrRouteUrl, arrRouteValid);
                let canAccess = permissionService.checkRoutePermissionV2(arrRouteUrl, arrRouteValid);

                if (canAccess) {
                    next();
                }
            }

            return response.send(responseUtil.response(null, responseUtil.FAIL_AUTHENTICATE_CODE, responseUtil.FAIL_AUTHENTICATE_MESSAGE));
        }).catch(function (error) {
            logger.logError(error.message);
            response.send(responseUtil.response(null, responseUtil.FAIL_AUTHENTICATE_CODE, error.message));
        });

    }).catch(function (error) {
        logger.logError(error.message);
        response.send(responseUtil.response(null, responseUtil.FAIL_AUTHENTICATE_CODE, error.message));
    })
};