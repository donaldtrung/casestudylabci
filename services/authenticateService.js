let bcrypt = require('bcryptjs');
let crypto = require('crypto');
let randomstring = require('randomstring');

let model = require('../models');
let jwt = require('jsonwebtoken');
let responseUtil = require('../utils/responseUtil');

let userModel = model.User;
let sessionModel = model.Session;
let clientCredentialModel = model.ClientCredentials;

exports.getClientSecret = function (clientId) {
    return new Promise(function (resolve, reject) {

        let filter = {
            where: {
                clientId: clientId
            },
            attributes: ['clientSecret']
        };

        clientCredentialModel.findOne(filter).then(function (record) {
            resolve(record);

        }).catch(function (error) {
            reject(error);
        });
    });
};


exports.checkLogin = function (username, password) {
    return new Promise(function (resolve, reject) {
        var filter = {
            where: {
                username: username
            }
        };

        userModel.findOne(filter).then(function (records) {
            if (!records) {
                reject({message: responseUtil.FAIL_LOGIN_MESSAGE});
            }

            let hash = records.dataValues.password;
            bcrypt.compareSync(password, hash) ? resolve(records) : reject({message: responseUtil.FAIL_LOGIN_MESSAGE});
        }).catch(function (error) {
            reject(error);
        });
    });
};

exports.handleSession = function (user) {
    return new Promise(function (resolve, reject) {
        let newSession = generateSession(user);

        let criterias = {
            session: newSession,
            userId: user.id
        };

        sessionModel.create(criterias).then(function (result) {
            resolve(newSession);

        }).catch(function (error) {
            reject(error);
        });
    });
};

function generateSession(user) {
    return jwt.sign(
        {
            header: process.env.DEFAULT_HEADER_JWT || null,
            body: user
        }, global.clientSecret);
};

exports.generateSession = function (user) {
    return jwt.sign(
        {
            header: process.env.DEFAULT_HEADER_JWT || null,
            body: user
        }, global.clientSecret);
}

exports.getSession = function (session) {
    return new Promise(function (resolve, reject) {

        let filter = {
            where: {
                session: session
            }
        };

        sessionModel.findOne(filter).then(function (record) {
            resolve(record);

        }).catch(function (error) {
            reject(error);
        });
    });
};

exports.removeSession = function (session) {
    return new Promise(function (resolve, reject) {

        let filter = {
            where: {
                session: session
            }
        };

        sessionModel.destroy(filter).then(function (record) {
            resolve(record);

        }).catch(function (error) {
            reject(error);
        });
    });
};

// node commands/generate-client-credential.js ios
exports.generationAuthencationKey = function (clientName) {
    return new Promise(function (resolve, reject) {
        let randomStr = randomstring.generate();

        randomStr = clientName + randomStr + Math.round(new Date() / 1000);
        let clientId = crypto.createHash('sha256').update(randomStr).digest('hex');

        randomStr = clientId + randomstring.generate() + Math.round(new Date() / 1000);
        let clientSecret = crypto.createHash('sha256').update(randomStr).digest('hex');

        let criterias = {
            clientId: clientId,
            clientSecret: clientSecret,
            clientName: clientName
        };

        clientCredentialModel.create(criterias).then(function (result) {
            console.log('Generate key successfully');
            console.log('clientName: ' + clientName + ' clientId: ' + clientId + ' clientSecret: ' + clientSecret);

            resolve(result);

        }).catch(function (error) {
            console.log(error.message);
            reject(error);
        });
    });
};

// var hash = bcrypt.hashSync('123456', salt)
//         console.log(`Auto-gen: ${hash}`)
